﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Calidad;

namespace CalidadTest
{
   public class Eje1
    {
        [Test]
        public void Rom01() {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3, 1000 });
            Assert.AreEqual(1000, result);


        }
        [Test]
        public void Rom02()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3,  1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom03()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom04()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1,1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom05()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 0, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom06()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom07()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom08()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom09()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 3, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom10()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] {1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom11()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2,  1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom12()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom13()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        [Test]
        public void Rom014()
        {

            var au = new Eje1();
            var result = au.Rom(new int[] { 1, 2, 3, 4, 1000 });
            Assert.AreEqual(1000, result);

        }
        
        private object Rom(int[] vs)
        {
            throw new NotImplementedException();
        }
    }
}
